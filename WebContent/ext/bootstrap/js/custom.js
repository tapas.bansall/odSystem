$(window).resize(function(){
    scaling();
});

function scaling(){
	var listwidth = $('.container-inner .navbar .nav li').width();
    $('.container-inner .navbar .nav li a').css('width',listwidth);

    var winheight = $( window ).height();
    $('.main-container').css('min-height',winheight-100);
    $('.dashboard-menu-item, .panel').css('height',(winheight-100)/2);
};

$(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();

	$('.document-box').click(function() {
		$('.document-option').slideToggle();
	});

	var textcount = 14;
	$('.chartwrap-bottom .chart-box').filter(function() {
	    return $(this).text().length < textcount;
	}).addClass('limitedtext');



	$('.navbar li').on('click', function() {
		var id = $(this).attr('id');
		var breadname = $(this).text();
		newid=id.replace('tab-','');

		$('.breadcrumb-item.active').text(breadname);
		

		$('.navbar li').removeClass('active');
		$(this).addClass('active');
		$('.tabtitle').removeClass('active');
		$('#mobtab-'+newid).addClass('active');
		$('.tabcontent').hide();
		$('#tabcon-'+newid).fadeIn();

		$('.navbar li').removeClass('completed');
		$('.tabtitle').removeClass('completed');
		for (i = 1; i < newid; i++) {
			$('#tab-'+i).each(function(){
			$(this).addClass('completed');
			});
			$('#mobtab-'+i).each(function(){
			$(this).addClass('completed');
			});
		}	
	});

	$('.tabtitle').on('click', function() {
		var id = $(this).attr('id');
		var breadname = $(this).text();
		newid=id.replace('mobtab-','');

		$('.breadcrumb-item.active').text(breadname);

		$('.navbar li').removeClass('active');
		$(this).addClass('active');
		$('.tabtitle').removeClass('active');
		$('#mobtab-'+newid).addClass('active');
		$('.tabcontent').hide();
		$('#tabcon-'+newid).fadeIn();

		$('.navbar li').removeClass('completed');
		$('.tabtitle').removeClass('completed');
		for (i = 1; i < newid; i++) {
			$('#tab-'+i).each(function(){
			$(this).addClass('completed');
			});
			$('#mobtab-'+i).each(function(){
			$(this).addClass('completed');
			});
		}	
	});

	$('.uploadTrigger').on('click', function() {
		var parent = $(this).parent();
		var uploadEl = $(parent).closest('.uploadElement');
		$(uploadEl).trigger('click');
	});

	scaling();
});