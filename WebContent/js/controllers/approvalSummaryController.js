var odSystem = angular.module('odSystem');
odSystem.controller('approvalSummaryController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    //something here
    // $scope.getReviewCount();
    $scope.approverReview = {
      overallComments: ''
    };
    $scope.viewableComments = {
      legislationComments: '',
      structureComments: '',
      functionalDescriptionComments: '',
      employeeComments: '',
      expenseComments: '',
      documentComments: ''
    };
  };

  $scope.getReviewStatus = function () {
    $http.get('http://localhost:8809/path-to-api', {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.approverReview.requestId = data.requestId;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.onSubmit = function (status) {
    $scope.approverReview.status = status;
    var data = JSON.stringify($scope.approverReview);
    $http.post('http://172.16.8.248:5555/rest/restServices/selectCUR', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.reviewSuccessfull = true;
      $scope.message = data.message;
    }).error(function (error) {
      $scope.reviewFailed = true;
      $scope.message = 'Request could not be updated at this time! Please try again later.';
    });
  };

  $scope.setViewableComments = function (comments) {
    $scope.viewableComments = comments;
    
  };

  $scope.viewRequest = function () {
    window.location.hash = '/viewRequest';
  }

  $scope.init();
}]);