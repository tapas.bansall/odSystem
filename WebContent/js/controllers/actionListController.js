var odSystem = angular.module('odSystem');
odSystem.controller('actionListController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    angular.element(document).ready(function() {
      window.setTimeout(function() {
        $('.multiselect-ui').multiselect({
          includeSelectAllOption: true
        });
      }, 500);
    });
    $scope.getAllActionList();
  };

  $scope.getAllActionList = function () {
  	// $http.get('http://172.16.8.248:5555/rest/restServices/selectCUR?userId' + $rootScope.loggedUser.userId, {
   //    headers: { 'Content-Type': 'application/json'},
   //    transformRequest: angular.identity
   //  }).success(function (data, status, headers, config) {
   //    $scope.templates = data;
   //.   $scope.notification.template = 0;
   //  }).error(function (error) {
   //    console.log(error);
   //  });
    $scope.getDummyData();

  };

  $scope.getDummyData = function () {
    $scope.data = [{
      subject: 'DOF- 23145- status change notification',
      message: 'Dear %User_group%,\n\nPlease note that the request to amend the organizational structure of %Entity_name% has been internally approved. The request has been received by TEC for initial review.\nPlease view your request details and the review team comments using the following link %Reference_Num%.\n\nThank you,\n\nThe Executive Council',
      date: '30/04/2017'
    }, {
      subject: 'SLC- 12316- status change notification',
      message: 'Dear %User_group%,\n\nPlease note that the request to amend the organizational structure of %Entity_name% has been submitted.\n\nPlease review and approve the request so it is received by The Executive Council.\n\nPlease view your request details and the review team comments using the following link %Reference_Num%.\n\nThank you,\n\nThe Executive Council',
      date: '11/09/2017'
    }];
  };

  $scope.init();
}]);