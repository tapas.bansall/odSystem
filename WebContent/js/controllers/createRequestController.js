var odSystem = angular.module('odSystem');
odSystem.controller('createRequestController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
	  $scope.$on('sessionExpired', function(event, obj) {
      $scope.draftRequest(false);
    });
    

    $scope.data = {
      governmentEntity: '',
      justificationToRequest: '',
      currentStructureNotHelping: '',
      changeType: 'major',
      restructuring: false,
      sectorSubChange: 'Add new sector',
      departmentSubChange: 'Add new department',
      sectionSubChange: 'Add new section'
    };

    $scope.showAllTables = true;
    $scope.showFinancialImplication = true;
    if ($rootScope.focalPointDetails) {
  		angular.forEach($rootScope.focalPointDetails, function(value, key) {
  			$scope.data[key] = value;
  		});
  	}
    $scope.rowCount = 1;
    $scope.lastTab = false;
    $scope.regex = '^(\\+971)(2|3|4|6|7|9|50|55)\\d{7}$';
    $scope.tempDocument = {
      name: '',
      type: 'Link',
      file: '',
      comment: ''
    }
    $scope.uploadedDocuments = [{
      name: 'Approval Letter',
      type: 'Link',
      file: '',
      comment: ''
    }, {
      name: 'Strategic Plan',
      type: 'Link',
      file: '',
      comment: ''
    }, {
      name: 'Benchmark',
      type: 'Link',
      file: '',
      comment: ''
    }, {
      name: 'Workload Analysis',
      type: 'Link',
      file: '',
      comment: ''
    }];
    $scope.legislation = [];
    $scope.structureChanges = [];
    $scope.functionalDescription = [];
    $scope.employeeList = [];
    $scope.expense = [];
    for (var i=0;i<$scope.rowCount;i++) {
      $scope.addRow();
    }
    angular.element(document).ready(function() {
      window.setTimeout(function() {
        $('.multiselect-ui').multiselect({
          includeSelectAllOption: true
        });
      }, 500);
    });
    $scope.getEntities();
    $scope.currentTab = 1;
  };

  $scope.getEntities = function () {
    // $http.get('http://localhost:8809/path-to-api', {
    //   headers: { 'Content-Type': 'application/json'},
    //   transformRequest: angular.identity
    // }).success(function (data, status, headers, config) {
    //   $scope.entities = data;
    // }).error(function (error) {
    //   console.log(error);
    // });
    $scope.entities = [{
      name: 'Road and transport authority',
      id: 'RTA'
    }, {
      name: 'Dubai sports council',
      id: 'DubaiSC'
    }, {
      name: 'Dubai Health Authority',
      id: 'DHA'
    }, {
      name: 'Dubai Women Empowerment',
      id: 'DWE'
    }];
    if ($rootScope.loggedUser.role === 'GE Coordinator') {
      $scope.data.governmentEntity = $rootScope.loggedUser.departmentId;
    } else {
      $scope.data.governmentEntity = $scope.entities[0]['id'];
    }
  };

  $scope.checkDefaultGE = function () {
    if ($rootScope.loggedUser.role === 'GE Coordinator') {
      return true;
    }
    return false;
  };

  $scope.$watch(function(scope) {
    return scope.data.changeType;
  }, function (newValue, oldValue) {
    if (newValue === 'major') {
      $scope.showAllTables = true;
      $scope.showFinancialImplication = true;
    } else {
      $scope.showAllTables = false;
    }
  });

  $scope.$watch(function(scope) {
    return scope.data.sectionChange;
  }, function (newValue, oldValue) {
    if ($scope.data.changeType === 'minor') {
      if (newValue) {
        $scope.showAllTables = true;
      } else {
        $scope.showAllTables = false;
      }
    }
  });

  $scope.$watch(function(scope) {
    return scope.data.restructuring;
  }, function (newValue, oldValue) {
    if(newValue) {
      $scope.data.sectorChange = false;
      $scope.data.departmentChange = false;
    }
  });

  $scope.openFileInput = function (id) {
    if(typeof id === 'number') {
      angular.element('#input-'+id).trigger('click');
    } else {
      angular.element('#'+id).trigger('click');
    }
  };

  $scope.onDocumentUploadSave = function () {
    $scope.uploadedDocuments.push($scope.tempDocument);
    $scope.tempDocument = {
      name: '',
      type: 'Link',
      file: '',
      comment: ''
    }
  };

  $scope.deleteFileFromUpload = function (index) {
    $scope.uploadedDocuments.splice(index, 1);
  };

  $scope.onSubmit = function (form) {
    if(form.$valid && $scope.validateTabs() && $scope.validateUploadTab()) {
      $scope.isFormInvalid = false;
      $scope.data.status = 2;
      $scope.sendRequest();
    } else {
      $scope.isFormInvalid = true;
      $('body').scrollTop(0);
    }
  };

  $scope.sendRequest = function (showMessage) {
    var data = $scope.data;
    data.legislation = $scope.legislation;
    data.functionalDescription = $scope.functionalDescription;
    data.expenseDetails = $scope.expense;
    data.structureChanges = $scope.structureChanges;
    data.manPowerDistribution = $scope.employeeList;
    data.uploadedDocuments = $scope.uploadedDocuments;
    data.userId = $rootScope.loggedUser.userId;
    data = JSON.stringify(data);
    $http.post('http://172.16.8.248:5555/rest/restServices/cur', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      if ($scope.data.status === 2) {
        $scope.hideButtons = true;
        $scope.response = data;
        $('#submitModal').modal('show');
      } else {
        $scope.data.requestId = data.requestId;
        if (showMessage) {
          $('#submitModal').modal('show');
        }
      }
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.draftRequest = function (showMessage) {
    $scope.data.status = 1;
    $scope.sendRequest(showMessage);
  };

  // $scope.deleteFile = function (name) {
  //   console.log($scope.uploadedDocuments);
  //   $scope.uploadedDocuments[name] = null;
  // };

  $scope.activeMinor = function () {
    $('#financialConfirmationModal').modal('show');
    $scope.data.changeType = 'minor';
    $scope.data.nameChange = false;
    $scope.data.restructuring = false;
    $scope.data.sectorChange = false;
    $scope.data.departmentChange = false;
  };

  $scope.activeMajor = function () {
    $scope.data.changeType = 'major';
    $scope.data.nameChange = false;
    $scope.data.restructuring = false;
    $scope.data.sectorChange = false;
    $scope.data.departmentChange = false;
  };

  $scope.tabChanged = function (evt) {
    $scope.draftRequest(false);
    var tmp = $scope.currentTab;
    if (tmp != '1' && tmp != '8') {
      $scope.tabValidation(tmp);
    } else if (tmp == '8') {
      $scope.validateUploadTab();
    }
    $scope.replicateValues(tmp);
    var id = $(evt.target).parent().attr('id');
    if (parseInt(id.replace('tab-',''), 10) === 8) {
      $scope.lastTab = true;
    } else {
      $scope.lastTab = false;
    }
    var breadname = $(evt.target).parent().text();
    newid = id.replace('tab-', '');
    $('.breadcrumb-item.active').text(breadname);
    $('.navbar li, .tabtitle').removeClass('active');
    $(evt.target).parent().addClass('active');
    $('.tabcontent').hide();
    $('#tabcon-' + newid).fadeIn();
    $scope.currentTab = id.replace('tab-','');
    $scope.getEmptyColumns($scope.currentTab);
  };

  $scope.tabValidation = function (id) {
    var valid = true;
    var tab = '#tabcon-' + id;
    var variable = angular.element(tab).data('var');
    if (variable) {
      for(var i =0; i<$scope[variable].length; i++) {
        angular.forEach($scope[variable][i], function(value, key) {
          if (typeof value === "string") {
            if (value && value.length > 0) {
              //continue
            } else {
              valid = false;
            }
          } else if(value === 0) {
            //continue
          } else {
            if (value && value > -1) {
              //continue
            } else {
              valid = false;
            }
          }
        });
      }
    }
    if (valid) {
      angular.element('#tab-' + id).addClass('completed');
    } else {
      angular.element('#tab-' + id).removeClass('completed');
    }
    return valid;
  };

  $scope.getEmptyColumns = function (id) {
      var emptyCells = [];
      var tab = '#tabcon-' + id;
      var variable = angular.element(tab).data('var');
      if (variable) {
        for (var i=0;i<$scope[variable].length;i++) {
          var tmp = [];
          angular.element(tab + ' thead td').each(function () {
            var subTitle = angular.element(this).data('var');
            var val = $scope[variable][i][subTitle];
            if ((typeof val === 'string' && val.length === 0) || (val === null)) {
              tmp.push(angular.element(this).text())
            }
        });
      }
      emptyCells.push(tmp);
    }
    var msg = [];
    emptyCells.forEach(function(row, index) {
      if(row.length > 0) {
        var msgrow = row.join(', ') + ' of row '+ (parseInt(index) + 1).toString() +' are missing.';
        msg.push(msgrow);
      }
    });
    if(msg.length > 0) {
      $scope.showInvalidColumns = true;
    } else {
      $scope.showInvalidColumns = false;
    }
    $scope.invalidColumns = msg;
  };

  $scope.replicateValues = function (id) {
    var tab = '#tabcon-' + id;
    var variable = angular.element(tab).data('var');
    if (variable && (variable === 'structureChanges' || variable === 'functionalDescription')) {
      for (var i=0;i<$scope.legislation.length;i++) {
        $scope.structureChanges[i]['newName']=$scope.functionalDescription[i]['newName']=$scope[variable][i]['newName'];
        $scope.structureChanges[i]['newLevel']=$scope.functionalDescription[i]['newLevel']=$scope[variable][i]['newLevel'];
        $scope.structureChanges[i]['newParent']=$scope.functionalDescription[i]['newParent']=$scope[variable][i]['newParent'];
      }
    }
  };

  $scope.addRow = function () {
    if($scope.currentTab == 5) {
      $scope.functionalDescription.push({
        newName: '',
        newLevel: '',
        newParent: '',
        currentFunction: '',
        proposedFunction: ''
      });
    } else {
      $scope.legislation.push({
        scope: '',
        objective: '',
        orgUnitStrategicObjective: ''
      });
      $scope.structureChanges.push({
        division: '',
        level: '',
        parent: '',
        headCount: '',
        changeType: '',
        newName: '',
        newLevel: '',
        newParent: '',
        newHeadCount: ''
      });
      $scope.functionalDescription.push({
        newName: '',
        newLevel: '',
        newParent: '',
        currentFunction: '',
        proposedFunction: ''
      });
      $scope.employeeList.push({
        newStructureId: '',
        newStructurePositionTitle: '',
        newStructureSector: '',
        newStructureDept: '',
        newStructureSection: '',
        newGrade: '',
        newExisting: '',
        typeOfContract: '',
        internalStaff: 0,
        externalHires: 0,
        newTotalCount: 0,
        currentPositionMapped: '',
        currentGrade: '',
        currentSalary: '',
        year1newCount: '',
        year2newCount: '',
        year3newCount: '',
        year1DirectCost: 0,
        year1IndirectCost: 0,
        year2DirectCost: 0,
        year2IndirectCost: 0,
        year3DirectCost: 0,
        year3IndirectCost: 0,
        totalCost: 0
      });
      $scope.expense.push({
        expenseType: '',
        totalCost: '',
        year1Cost: '',
        year2Cost: '',
        year3Cost: ''
      });
    }
  };

  $scope.calculateProposedTotalCount = function (i) {
    var obj = $scope.employeeList[i];
    var i = obj.internalStaff;
    i = (i==='' || i===null)?0:parseInt(i);
    var e = obj.externalHires;
    e = (e==='' || e===null)?0:parseInt(e);
    obj.newTotalCount = i+e;
  };

  $scope.deleteRow = function (index) {
    $scope.legislation.splice(index, 1);
    $scope.structureChanges.splice(index, 1);
    $scope.functionalDescription.splice(index, 1);
    $scope.employeeList.splice(index, 1);
    $scope.expense.splice(index, 1);
  }

  $scope.calculateTotalCost = function (index) {
    var obj = $scope.employeeList[index];
    var y1d = obj.year1DirectCost;
    y1d = (y1d===''||y1d===null)?0:parseInt(y1d);
    var y1ind = obj.year1IndirectCost;
    y1ind = (y1ind===''||y1ind===null)?0:parseInt(y1ind);
    var y2d = obj.year2DirectCost;
    y2d = (y2d===''||y2d===null)?0:parseInt(y2d);
    var y2ind = obj.year2IndirectCost;
    y2ind = (y2ind===''||y2ind===null)?0:parseInt(y2ind);
    var y3d = obj.year3DirectCost;
    y3d = (y3d===''||y3d===null)?0:parseInt(y3d);
    var y3ind = obj.year3IndirectCost;
    y3ind = (y3ind===''||y3ind===null)?0:parseInt(y3ind);
    obj.totalCost = y1d + y1ind + y2d + y2ind + y3d + y3ind;
  };

  $scope.validateTabs = function () {
    var valid = true;
    angular.element(".navbar li").each(function() {
      var id = angular.element(this).attr('id');
      id = id.replace('tab-', '');
      if(!$scope.tabValidation(id)) {
        valid = false;
        // break;
      }
    });
    return valid;
  };

  $scope.validateUploadTab = function () {
    var valid = true;
    $scope.uploadedDocuments.forEach(function(doc, index) {
      if(index < 4) {
        if(doc.file.length == 0) {
          valid = false;
        }
      }
    });
    if (valid) {
      angular.element('#tab-8').addClass('completed');
    } else {
      angular.element('#tab-8').removeClass('completed');
    }
    return valid;
  };

  $scope.validateRequestDetailsTab = function () {
    var valid = true;

  };

  $scope.financialConfirmationYes = function() {
    $('#financialConfirmationModal').modal('hide');
    $scope.showFinancialImplication = true;
  };

  $scope.financialConfirmationNo = function() {
    $('#financialConfirmationModal').modal('hide');
    $scope.showFinancialImplication = false;
  };

  $scope.exitClick = function () {
    $('#confirmationModal').modal('hide');
    $('#submitModal').modal('hide');
    window.location.hash = '/dashboard';
  };

  $scope.init();
}]);