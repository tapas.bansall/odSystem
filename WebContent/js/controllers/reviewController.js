var odSystem = angular.module('odSystem');
odSystem.controller('reviewController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {

  $scope.init = function () {
    //something here
    $scope.rowCount = 1;
	  $scope.review = {
      leadApproval: 'notRequired',
      preApproval: 'no',
      sector: 'Select',
      summary: [],
      legislationRemarks: '',
      structureChangesRemarks: '',
      functionalDescriptionRemarks: '',
      employeeListRemarks: '',
      expenseRemarks: ''
    };

    $scope.reviewUploadDocuments = {
      reviewDocument: null
    };
    $scope.uploadedDocumentsToSend = [];

    $scope.uploadedDocuments = [{
      type: 'Approval Letter',
      link: 'http://abc.com'
    }, {
      type: 'Strategic Plan',
      link: 'http://abc.com'
    }, {
      type: 'Benchmark',
      link: 'http://abc.com'
    }, {
      type: 'Workload Analysis',
      link: 'http://abc.com'
    }, {
      type: 'Other Documents',
      link: 'http://abc.com'
    }];

    $scope.hideNav = false;
    // $scope.getRequestDetails();
    $scope.getDummyData();

    // for(var i = 0; i<$scope.rowCount; i++){
    //   $scope.addRow();
    // }

    angular.element(document).ready(function() {
      window.setTimeout(function() {
        $('.multiselect-ui').multiselect({
          includeSelectAllOption: true
        });
      }, 1000);
    });
    if ($scope.checkApprovalPermission()) {
      setTimeout(function(){
        var breadname = $('#tab-9').text();
        $('.breadcrumb-item.active').text(breadname);
        $('.navbar li, .tabtitle').removeClass('active');
        $('#tab-9').addClass('active');
        $('.tabcontent').hide();
        $('#tabcon-9').fadeIn();
      }, 250);
    }
  };

  $scope.addRow = function() {
    $scope.review.summary.push({
      division: '',
      level: '',
      parent: '',
      decision: 'Approve',
      justification: '',
    });
  };

  $scope.getDummyData = function() {
    $scope.data = {};
    $scope.data.govermentEntity = 'Department of Finance';
    $scope.data.pointName = 'Lorem';
    $scope.data.pointNumber = '1234';
    $scope.data.pointEmail = 'email@email.com'
    $scope.data.pointDesignation = 'Lorem Ipsum';
    $scope.data.directorName = 'Ipsum';
    $scope.data.directorNumber = '12345';
    $scope.data.directorEmail = 'director@director.com';
    $scope.data.directorDesignation = 'Lorem';
    $scope.legislation = [];
    $scope.structureChanges = [];
    $scope.functionalDescription = [];
    $scope.employeeList = [];
    $scope.expense = [];
    $scope.legislation.push({
      newName: 'Lorem',
      newLevel: 'Sector',
      newParent: 'Ipsum',
      scope: 'Lorem',
      objective: 'Ipsum',
      orgUnitStrategicObjective: 'Lorem Ipsum'
    });
    $scope.structureChanges.push({
      division: 'Lorem',
      level: 'Sector',
      parent: 'Ipsum',
      headCount: '3',
      changeType: 'Amend roles & responsibilities',
      newName: 'Ipsum',
      newLevel: 'Ipsum',
      newParent: 'Ipsum',
      newHeadCount: '2',
      justification: 'Lorem Ipsum'
    });
    $scope.functionalDescription.push({
      newName: 'Lorem',
      newLevel: 'Sector',
      newParent: 'Ipsum',
      currentFunction: 'Lorem',
      proposedFunction: 'Ipsum'
    });
    $scope.employeeList.push({
      newStructureId: '123',
      newStructurePositionTitle: 'Lorem',
      newStructureSector: 'Ipsum',
      newStructureDept: 'Lorem',
      newStructureSection: 'Ipsum',
      newGrade: '123',
      newExisting: 'Existing',
      typeOfContract: 'Lorem',
      internalStaff: 'Lorem',
      externalHires: 'Ipsum',
      newTotalCount: '123',
      currentPositionMapped: 'Lorem',
      currentGrade: '123',
      currentSalary: '123',
      year1newCount: '123',
      year2newCount: '123',
      year3newCount: '123',
      year1DirectCost: '123',
      year1IndirectCost: '123',
      year2DirectCost: '123',
      year2IndirectCost: '123',
      year3DirectCost: '123',
      year3IndirectCost: '123',
      totalCost: '246'
    });
    $scope.expense.push({
      expenseType: 'Lorem',
      totalCost: '300',
      year1Cost: '100',
      year2Cost: '100',
      year3Cost: '100'
    }); 
    // $scope.addRow();
    $scope.review.summary.push({
      newName: 'Lorem',
      changeType: 'New',
      division: 'Ipsum',
      decision: 'Approve',
      justification: '',
    });
  };

  $scope.getRequestDetails = function () {
    var requestId = $rootScope.currentRequest;
    $http.get('http://172.16.8.248:5555/rest/restServices/selectCUR?userId' + $rootScope.loggedUser.userId, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
      $scope.review.requestId = data.requestId;
      $scope.functionalDescription = data.functionalDescription;
      $scope.legislation = data.legislation;
      $scope.structureChanges = data.structureChanges;
      $scope.employeeList = data.employeeList;
      $scope.expense = data.expense;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.postReview = function (status) {
    $scope.review.status = status;
    $scope.review.userId = $rootScope.loggedUser.userId;
    $scope.review.uploadDocuments = $scope.uploadedDocumentsToSend;
    var data = JSON.stringify($scope.review);
    $http.post('http://172.16.8.248:5555/rest/restServices/selectCUR', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.reviewSuccessfull = true;
      $scope.message = data.message;
    }).error(function (error) {
      $scope.reviewFailed = true;
      $scope.message = 'Request could not be updated at this time! Please try again later.';
    });
  };

  $scope.tabChanged = function (evt) {
    var id = $(evt.target).parent().attr('id');
    var lastId = $('.nav li:last').attr('id'); 
    lastId = lastId.replace('tab-', '');
    if (parseInt(id.replace('tab-',''), 10) === parseInt(lastId, 10)) {
      $scope.hideNav = true;
    } else {
      $scope.hideNav = false;
    }
    var breadname = $(evt.target).parent().text();
    newid = id.replace('tab-', '');
    $('.breadcrumb-item.active').text(breadname);
    $('.navbar li, .tabtitle').removeClass('active');
    $(evt.target).parent().addClass('active');
    $('.tabcontent').hide();
    $('#tabcon-' + newid).fadeIn();
  };

  $scope.nextTab = function () {
    var id = $('.nav li.active').attr('id');
    id = parseInt(id.replace('tab-',''), 10);
    var nextTab = 'tab-' + (id + 1);
    var lastId = $('.nav li:last').attr('id'); 
    lastId = lastId.replace('tab-', '');
    if(id + 1 === parseInt(lastId, 10)) {
      $scope.hideNav = true;
    }
    var breadname = $('#' + nextTab).text();
    $('.breadcrumb-item.active').text(breadname);
    $('.navbar li, .tabtitle').removeClass('active');
    $('#' + nextTab).addClass('active');
    $('.tabcontent').hide();
    $('#tabcon-' + (id + 1)).fadeIn();
  };

  $scope.openFileInput = function () {
    angular.element("#reviewDocument").val(null);
    $('#reviewDocument').trigger('click');
  };

  $scope.uploadDocument = function () {
    if($scope.reviewUploadDocuments.reviewDocument){
        $scope.uploadedDocumentsToSend.push(Object.assign({}, $scope.reviewUploadDocuments.reviewDocument));
    }
    $scope.reviewUploadDocuments.reviewDocument = null;
  };

  $scope.deleteFile = function (index) {
    $scope.uploadedDocumentsToSend.splice(index, 1);
  };

  $scope.exitClick = function () {
    $('#confirmationModal').modal('hide');
    window.history.back();
  };

  $scope.checkInitalApprovalPermission = function () {
    if ($rootScope.loggedUser && $rootScope.loggedUser.role === 'GE Approver') {
      return true;
    }
    return false;
  };

  $scope.checkRejectionPermission = function () {
    if ($rootScope.loggedUser.role === 'GE Approver' || $rootScope.loggedUser.role === 'TEC Coordinator') {
      return true;
    }
    return false;
  };

  $scope.checkValidationPermission = function () {
    if ($rootScope.loggedUser && $rootScope.loggedUser.role === 'TEC Coordinator') {
      return true;
    }
    return false;
  };

  $scope.checkSavePermission = function () {
    if ($rootScope.loggedUser.role === 'GE Approver' || $rootScope.loggedUser.role === 'TEC Approver') {
      return false;
    }
    return true;
  };

  $scope.checkReviewPermission = function () {
    if ($rootScope.loggedUser.role === 'GE Approver' || $rootScope.loggedUser.role === 'TEC Approver') {
      return false;
    }
    return true;
  };

  $scope.checkConsolidatePermission = function () {
    if ($rootScope.loggedUser && $rootScope.loggedUser.role === 'TEC Coordinator') {
      return true;
    }
    return false;
  };

  $scope.checkApprovalPermission = function () {
    if ($rootScope.loggedUser.role === 'TEC Approver') {
      return true;
    }
    return false;
  };

  $scope.checkCompletionPermission = function () {
    if ($rootScope.loggedUser && $rootScope.loggedUser.role === 'TEC Coordinator') {
      return true;
    }
    return false;
  };

  $scope.setViewableComments = function (comments) {
    $scope.viewableComments = comments;
    
  };

  $scope.init();
}]);