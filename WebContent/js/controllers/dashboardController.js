var odSystem = angular.module('odSystem');
odSystem.controller('dashboardController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    $scope.checkLogin();
    $scope.getNewRequestCount();
    $scope.getFocalPoint();
    angular.element(document).ready(function() {
      window.setTimeout(function() {
        $('.multiselect-ui').multiselect({
          includeSelectAllOption: true
        });
      }, 1000);
    });
    $(document).ready(function() {
      var menuCount = $('div.dashboard-menu-item').length;
      if (menuCount === 4) {
        $('.dashboard-menu-item').removeClass('col-lg-4 col-md-4 col-sm-4');
        $('.dashboard-menu-item').addClass('col-lg-6 col-md-6 col-sm-6');
        var winheight = $( window ).height();
        $('.dashboard-menu-item, .panel').css('height',(winheight-100)/2); 
      }
    });
  };

  $scope.getNewRequestCount = function() {
  	$http.get('http://172.16.8.248:5555/rest/restServices/selectCUR?userId' + $rootScope.loggedUser.userId, {
  		headers: { 'Content-Type': 'application/json'},
  		transformRequest: angular.identity
  	}).success(function (data, status, headers, config) {
  		$rootScope.newRequestCount = data.requestCount;
  	}).error(function (error) {
  		console.log(error);
  	});
  };
  
  $scope.getFocalPoint = function () {
	  	$http.get('http://172.16.8.248:5555/rest/services/getMasterData', {
	  		headers: { 'Content-Type': 'application/json'},
	  		transformRequest: angular.identity
	  	}).success(function (data, status, headers, config) {
	  		$rootScope.focalPointDetails = data;
	  	}).error(function (error) {
	  		console.log(error);
	  	});
  };

  $scope.checkRequestPermission = function () {
  	if ($rootScope.loggedUser.role === 'TEC Coordinator' || $rootScope.loggedUser.role === 'GE Coordinator') {
  		return true;
  	}
  	return false;
  };

  $scope.checkReviewPermission = function () {
  	if ($rootScope.loggedUser.role === 'GE Coordinator') {
  		return false;
  	}
  	return true;
  };

  $scope.checkNotificationPermission = function () {
  	if ($rootScope.loggedUser.role === 'TEC Coordinator' || $rootScope.loggedUser.role === 'TEC Approver') {
  		return true;
  	}
  	return false;
  };

  $scope.checkSectorLead = function () {
  	if ($rootScope.loggedUser.role === 'Sector Lead') {
  		return false;
  	}
  	return true;
  };

  $scope.checkLogin = function () {
    var tmp = window.localStorage.getItem('userDetails');
    if (tmp) {
      tmp = JSON.parse(tmp);
      $rootScope.loggedUser = tmp;
      $rootScope.userLogged = true;
      angular.element('body').removeClass('login-body');
    }
  };

  $scope.showNotification = function () {
    $('#notificationModal').modal('show');
  };

  $scope.sendNotification = function () {
    console.log($scope.notification);
  };

  $scope.navigateToMashzone = function () {
	  window.open('http://172.16.8.244:8080/presto/hub/dashboard/dashboard.jsp?guid=9f84e8d6-9924-4b87-a211-52f2a7d85ef1', '_blank');
  };
  $scope.init();
}]);