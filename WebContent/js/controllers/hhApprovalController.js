
var odSystem = angular.module('odSystem');
odSystem.controller('hhApprovalController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    $scope.uploadedDocuments = {
        approvalDocument: null
    };
    $scope.tempDocument = {
      name: '',
      type: 'Link',
      file: '',
      comment: ''
    }
    $scope.uploadedDocuments = [{
      name: 'Endorsement Letter',
      type: 'Link',
      file: '',
      comment: ''
    }];
    // $scope.uploadedDocumentsToSend = [];
    $scope.review = {
      overallComments: ''
    };
    $scope.getReviewStatus();
  };

  $scope.getReviewStatus = function () {
    $http.get('http://localhost:8809/path-to-api?userId' + $rootScope.loggedUser.userId, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.data = data;
    }).error(function (error) {
      console.log(error);
    });
  };

  $scope.openFileInput = function () {
    $('#approvalDocument').trigger('click');
  };

  // $scope.uploadDocument = function () {
  //   if($scope.uploadedDocuments.approvalDocument)
  //       $scope.uploadedDocumentsToSend.push($scope.uploadedDocuments.approvalDocument);
  //   $scope.uploadedDocuments.approvalDocument = null;
  //   console.log("SEND:", $scope.uploadedDocumentsToSend);
  // };

  // $scope.deleteFile = function (index) {
  //     $scope.uploadedDocumentsToSend.splice(index, 1);
  // };

  $scope.openFileInput = function (id) {
    if(typeof id === 'number') {
      angular.element('#input-'+id).trigger('click');
    } else {
      angular.element('#'+id).trigger('click');
    }
  };

  $scope.onDocumentUploadSave = function () {
    $scope.uploadedDocuments.push($scope.tempDocument);
    $scope.tempDocument = {
      name: '',
      type: 'Link',
      file: '',
      comment: ''
    }
  };

  $scope.deleteFileFromUpload = function (index) {
    $scope.uploadedDocuments.splice(index, 1);
  };

  $scope.onSubmit = function () {
    var formData = new FormData();
    formData.append('review', $scope.review.overallComments);
    formData.append('requestId', $scope.data.requestId);
    formData.append('userId', $rootScope.loggedUser.userId);
    // formData.append('legislationApprovalLetter', $scope.uploadedDocumentsToSend);
    formData.append('legislationApprovalLetter', $scope.uploadedDocuments);
    formData.append('status', 10);
    $http.post('http://172.16.8.248:5555/rest/restServices/selectCUR', data, {
        headers: { 'Content-Type': undefined},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.reviewSuccessfull = true;
      $scope.message = data.message;
    }).error(function (error) {
      $scope.reviewFailed = true;
      $scope.message = 'Request could not be updated at this time! Please try again later.';
    });
  }
  $scope.exitClick = function () {
    window.location.hash = '/reviewlisting';
  };

  $scope.init();
}]);