var odSystem = angular.module('odSystem');
odSystem.controller('trackController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    $scope.getRequests();
    $scope.searchData = {
      referenceNumber: '',
      dateOfSubmission: '',
      govermentEntity: '',
      status: ''
    }
  };

  $scope.getRequests = function () {
    // $http.get('http://localhost:8809/path-to-api?userId=' + $rootScope.loggedUser.userId, {
    //   headers: { 'Content-Type': 'application/json'},
    //   transformRequest: angular.identity
    // }).success(function (data, status, headers, config) {
    //   $scope.data = data;
    //   $scope.createPagination();
    //   $scope.handlePagination(1);
    // }).error(function (error) {
    //   console.log(error);
    // });
    $scope.data = [{
        referenceNumber: 1,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "Drafted",
        requestId: 1
      },
      {
        referenceNumber: 2,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Initiated",
        requestId: 1
      },
      {
        referenceNumber: 3,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "Created",
        requestId: 1
      },
      {
        referenceNumber: 4,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Validated",
        requestId: 1
      },
      {
        referenceNumber: 5,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "In-review",
        requestId: 1
      },
      {
        referenceNumber: 6,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Consolidated",
        requestId: 1
      },
      {
        referenceNumber: 7,
        department: "ABC",
        dataValue: "XYZ",
        date: '11/29/2017',
        status: "Approved",
        requestId: 1
      },
      {
        referenceNumber: 8,
        department: "ABCD",
        dataValue: "WXYZ",
        date: '11/29/2017',
        status: "Escalated",
        requestId: 1
      },
      {
        referenceNumber: 9,
        department: "ABCD",
        dataValue: "WXYZ",
        date: '11/28/2017',
        status: "Rejected",
        requestId: 1
      },
      {
        referenceNumber: 11,
        department: "ABCD",
        dataValue: "WXYZ",
        date: '11/27/2017',
        status: "HH Approved",
        requestId: 1
      },
      {
        referenceNumber: 12,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Completed",
        requestId: 1
      }];
  };

  $scope.openRequest = function (id) {
    $rootScope.currentRequest = id;
    window.location.hash = '/viewRequest';
  };

  $scope.filterByStatus = function(request) {
    if ($scope.searchData && $scope.searchData.status && $scope.searchData.status.length > 0) {
      return (request.status.indexOf($scope.searchData.status) !== -1);
    } else {
      return true;
    }
  };

  $scope.filterByDate = function(request) {
    if ($scope.searchData && $scope.searchData.dateOfSubmission) {
      var date = new Date($scope.searchData.dateOfSubmission);
      var dateString = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
      return (request.date === dateString);
    } else {
      return true;
    }
  };

  $scope.filterByGE = function(request) {
    if ($scope.searchData && $scope.searchData.govermentEntity && $scope.searchData.govermentEntity.length > 0) {
      var dep = request.department.toLowerCase();
      var filter = $scope.searchData.govermentEntity.toLowerCase();
      return (dep.indexOf(filter) > -1);
    } else {
      return true;
    }
  };

  $scope.filterByRef = function(request) {
    if ($scope.searchData && $scope.searchData.referenceNumber) {
      var ref = parseInt(request.referenceNumber, 10);
      var filter = parseInt($scope.searchData.referenceNumber, 10);
      return (ref == filter);
    } else {
      return true;
    }
  };

  $scope.init();
}]);