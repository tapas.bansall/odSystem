var odSystem = angular.module('odSystem');
odSystem.controller('mainController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    $rootScope.newRequestCount = 0;
    angular.element(document).on('click keypress', function() {
      $rootScope.sessionTimer = 300;
    });
    $scope.application = {
      language : 'en'
    };
    $scope.system = window.system;
  };

  $scope.getTemplatePath = function (templateName, subfolder) {
    if (subfolder) {
      return $scope.system + 'components/' + subfolder + '/' + templateName + '.html';
    }
    return $scope.system + 'components/' + templateName + '.html';
  };

  $scope.navigateTo = function (location) {
    window.location.hash = '/' + location;
  };

  $scope.logout = function () {
    $scope.$broadcast('sessionExpired', {});
    window.setTimeout(function() {
      $rootScope.loggedUser = null;
      $rootScope.userLogged = false;
      window.localStorage.removeItem('userDetails');
      $scope.navigateTo('');
      window.clearInterval($rootScope.sessionInterval);
      $('#sessionModal').modal('hide');
    }, 1000);
  };

  $scope.redirectToHome = function () {
    if ($rootScope.userLogged) {
      $scope.navigateTo('dashboard');
    } else {
      $scope.navigateTo('');
    }
  };

  $scope.sessionModalClose = function () {
    $scope.logout();
  };

  $scope.init();
}]);