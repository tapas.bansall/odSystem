var odSystem = angular.module('odSystem');
odSystem.controller('loginController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    angular.element('body').addClass('login-body');
    $scope.data = {
      language: 'english',
      username: '',
      password: ''
    };

    $scope.checkLogin();

    $scope.displayError = false;

    $scope.defaultCredentials = [{
      name: 'Mohammed Salim',
      role: 'GE Coordinator',
      password: 'salim@123',
      username: 'salim@tec.gov.ae',
      department: 'Department of Finance',
      departmentId: 'RTA',
      userId: 20
    }, {
      name: 'Sami Mohammed Ali',
      role: 'TEC Coordinator',
      password: 'sami@123',
      username: 'sami@tec.gov.ae',
      department: 'Governance Department',
      userId: 22 
    }, {
      name: 'Hamdah Bin Kalban',
      role: 'TEC Approver',
      password: 'hamdah@123',
      username: 'hamdah@tec.gov.ae',
      department: 'Governance Department',
      userId: 23
    }, {
      name: 'Mohammed Abdul',
      role: 'GE Approver',
      password: 'mohammed@123',
      username: 'Mohammadabdul@tec.gov.ae',
      department: 'Department of Finance',
      departmentId: 'RTA',
      userId: 21
    }, {
      name: 'Mohammed Aletawi',
      role: 'SLC Coordinator',
      password: 'mohammad@123',
      username: 'Mohammed.AlEtawi@slc.dubai.gov.ae',
      department: 'Governance Department',
      userId: 26
    }, {
      name: 'Hala Salem',
      role: 'Sector Lead',
      password: 'hala@123',
      username: 'Hala.salem@tec.gov.ae',
      department: 'Governance Department',
      userId: 25
    }];
    //setting default session as 5 minutes
    $rootScope.sessionTimer = 300;
  };

  $scope.onSubmit = function () {
    for (var i = 0; i < $scope.defaultCredentials.length; i++) {
      var tmp = $scope.defaultCredentials[i];
      if ($scope.data.username == tmp.username && $scope.data.password == tmp.password) {
        $rootScope.loggedUser = tmp;
        window.localStorage.setItem('userDetails', JSON.stringify(tmp));
        $rootScope.userLogged = true;
        $scope.displayError = false;
        $scope.navigateTo('dashboard');
        angular.element('body').removeClass('login-body');
        $scope.addCounterForSession();
        break;
      } else {
        $scope.displaySuccess = false;
        $scope.displayError = true;
      }
    }
  };

  $scope.checkLogin = function () {
    var tmp = window.localStorage.getItem('userDetails');
    if (tmp) {
      tmp = JSON.parse(tmp);
      $rootScope.loggedUser = tmp;
      $rootScope.userLogged = true;
      $scope.navigateTo('dashboard');
      angular.element('body').removeClass('login-body');
      $scope.addCounterForSession();
    }
  };

  $scope.addCounterForSession = function () {
    $rootScope.sessionInterval = window.setInterval(function() {
      $rootScope.sessionTimer -= 10;
      if ($rootScope.sessionTimer < 1) {
        $rootScope.sessionExpired = true;
        $('#sessionModal').modal('show');
      }
    }, 10000);
  };

  $scope.init();
}]);