var odSystem = angular.module('odSystem');
odSystem.controller('reviewListingController', ['$scope', '$rootScope', '$http', function ($scope, $rootScope, $http) {
  $scope.init = function () {
    $scope.getRequests();
  };

  $scope.getRequests = function () {
    var userId = $rootScope.loggedUser.userId;
    // $http.get('http://localhost:8809/path-to-api?userId' + userId, {
    //   headers: { 'Content-Type': 'application/json'},
    //   transformRequest: angular.identity
    // }).success(function (data, status, headers, config) {
    //   $scope.data = data;
    // }).error(function (error) {
    //   console.log(error);
    // });
    $scope.data = [{
        referenceNumber: 1,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "Pending",
        requestId: 1
      },
      {
        referenceNumber: 2,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Approved",
        requestId: 1
      },
      {
        referenceNumber: 3,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "Pending",
        requestId: 1
      },
      {
        referenceNumber: 4,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Approved",
        requestId: 1
      },
      {
        referenceNumber: 5,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "Pending",
        requestId: 1
      },
      {
        referenceNumber: 6,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Approved",
        requestId: 1
      },
      {
        referenceNumber: 7,
        department: "ABC",
        dataValue: "XYZ",
        date: 123,
        status: "Pending",
        requestId: 1
      },
      {
        referenceNumber: 8,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Approved",
        requestId: 1
      },
      {
        referenceNumber: 9,
        department: "ABCD",
        dataValue: "WXYZ",
        date: 1234,
        status: "Approved",
        requestId: 1
      }];
  };

  $scope.openRequest = function (id) {
    $rootScope.currentRequest = id;
    window.location.hash = '/viewRequest';
  };

  $scope.updateRequest = function (record, status) {
    var tmp = {};
    angular.copy(record, tmp);
    console.log(tmp);
    record.status = status;
    record.userId = $rootScope.loggedUser.userId;
    var data = JSON.stringify(record);
    $http.post('http://172.16.8.248:5555/rest/restServices/selectCUR', data, {
        headers: { 'Content-Type': 'application/json'},
        transformRequest: angular.identity
    }).success(function (data, status, headers, config) {
      $scope.reviewSuccessfull = true;
      $scope.message = data.message;
    }).error(function (error) {
      $scope.reviewFailed = true;
      $scope.message = 'Request could not be updated at this time! Please try again later.';
    });
  };

  $scope.checkDirectApproval = function (record) {
    if (record) {
      if (record.status === 'Pending' && $rootScope.loggedUser.role === 'GE Approver') {
        return true;
      }
    } else {
      if ($rootScope.loggedUser.role === 'GE Approver') {
        return true;
      }
    }
    return false;
  };

  $scope.init();
}]);