/*global angular */

/**
 * The main Account Management app module
 *
 * @type {angular.Module}
 */
//var system = 'odSystem/';
window.system = './';
var odSystem = angular.module('odSystem', ['ngRoute', 'angularjsFileModel']);
odSystem.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/', {
        controller: 'loginController',
        templateUrl: window.system + 'pages/login.html',
        header: false,
        footer: true,
        login: true
    }).when('/dashboard', {
        controller: 'dashboardController',
        templateUrl: window.system + 'pages/dashboard.html',
        header: true,
        footer: true
    }).when('/createRequest', {
        controller: 'createRequestController',
        templateUrl: window.system + 'pages/createRequest.html',
        header: true,
        footer: true
    }).when('/reviewlisting', {
        controller: 'reviewListingController',
        templateUrl: window.system + 'pages/reviewListing.html',
        header: true,
        footer: true
    }).when('/viewRequest', {
        controller: 'reviewController',
        templateUrl: window.system + 'pages/viewRequest.html',
        header: true,
        footer: true
    }).when('/approveRequest', {
        controller: 'approvalSummaryController',
        templateUrl: window.system + 'pages/approvalSummary.html',
        header: true,
        footer: true
    }).when('/hhApproval', {
        controller: 'hhApprovalController',
        templateUrl: window.system + 'pages/hhApproval.html',
        header: true,
        footer: true
    }).when('/trackntrace',{
        controller: 'trackController',
        templateUrl: window.system + 'pages/trackntrace.html',
        header: true,
        footer: true
    }).when('/actionList',{
        controller: 'actionListController',
        templateUrl: window.system + 'pages/actionList.html',
        header: true,
        footer: true
    }).when('/notifications',{
        controller: 'notificationController',
        templateUrl: window.system + 'pages/notifications.html',
        header: true,
        footer: true
    }).when('/aboutOD',{
        templateUrl: window.system + 'pages/aboutOD.html',
        header: true,
        footer: true
    }).when('/publishCommunication',{
        controller: 'notificationController',
        templateUrl: window.system + 'pages/publishNotification.html',
        header: true,
        footer: true
    }).otherwise({
        redirectTo: '/'
    });
    window.routeProvider = $routeProvider;
}]);

odSystem.run(function($rootScope, $location) {
  // register listener to watch route changes
  $rootScope.$on("$routeChangeStart", function(event, next, current) {
    if (next.$$route.originalPath !== '' && next.$$route.originalPath !== '/aboutOD') {
      if ($rootScope.userLogged === undefined || !$rootScope.userLogged) {
        $location.path("/");
      }
    }
    if (next.$$route.originalPath === '/aboutOD') {
        angular.element('body').removeClass('login-body');
    } else if (!$rootScope.userLogged) {
        angular.element('body').addClass('login-body');
    }
    $rootScope.header = next.header;
    $rootScope.footer = next.footer;
    $rootScope.login = next.login;
  });
});

var setUpProject = function() {
  angular.element('.moduleContainer').each(function(index, moduleContainer) {
    angular.bootstrap(moduleContainer, angular.element(moduleContainer).data('ngModules'));
  });
}
$(document).ready(setUpProject);